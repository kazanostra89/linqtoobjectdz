﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToObject
{
    class Program
    {
        static void Main(string[] args)
        {
            #region LinqBegin1

            List<int> list = new List<int>() { 1, 0, -3, 5, -1, 8, 2 };

            Console.WriteLine(list.First(x => x > 0));
            Console.WriteLine(list.Last(x => x < 0));
            Console.WriteLine(list.FindLast(x => x < 0));

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 2");
            #region LinqBegin2

            /*Random rnd = new Random();
            //List<int> list2 = null;*/
            List<int> list2 = new List<int>() { -1, -5, 1525, 840, 5, -98 };
            //list2 = GeneratreList(list2, rnd);
            //int d = InputInt("Введите положительное число от 1 до 9 включительно: ");
            PrintList(list2);
           
            Console.WriteLine(list2.FirstOrDefault(x => x > 0 && x % 10 == 5));
            #endregion
            Console.WriteLine(new string('*', 35) + " >> 3");
            #region LinqBegin3

            List<string> list3 = new List<string>() { "1Kopl", "1Ro", "5Ruslan" };

            Console.WriteLine(list3.FindLast(item3 => item3.Length > 3 && item3[0] >= 48 && item3[0] <= 57) ?? "Not found");

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 4");
            #region LinqBegin4

            List<string> list4 = new List<string>() { "Qwk", "o1", "dk" };

            /*Console.WriteLine(">>>" + list4.Count(item4 => item4.EndsWith("k", StringComparison.CurrentCultureIgnoreCase)));*/

            if (list4.Count(item4 => item4.EndsWith("k", StringComparison.CurrentCultureIgnoreCase)) == 1)
            {
                Console.WriteLine(">>>" + list4.Find(item41 => item41.EndsWith("k", StringComparison.CurrentCultureIgnoreCase)));
            }
            else if (list4.Count(item4 => item4.EndsWith("k", StringComparison.CurrentCultureIgnoreCase)) == 0)
            {
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Error!");
            }

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 5");
            #region LinqBegin5-6

            List<string> list5 = new List<string>() { "alena", "aa", "sdsDada" };

            Console.WriteLine("Совпадение символа: " + list5.Count(item5 => item5.Length > 1 && item5.StartsWith("A", StringComparison.CurrentCultureIgnoreCase) && item5.EndsWith("A", StringComparison.CurrentCultureIgnoreCase)).ToString() ?? "Not found");

            Console.WriteLine(new string('*', 35) + " >> 6");

            Console.WriteLine("Сумма значений коллекции: " + list5.Sum(item6 => item6.Length));

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 7");
            #region LinqBegin7

            List<int> list7 = new List<int>() { 1, -2, -1, -2, -5 };

            Console.WriteLine(list7.Count(item7 => item7 < 0).ToString() ?? "0");

            Console.WriteLine(list7.Where(item71 => item71 < 0).Sum().ToString() ?? "0");

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 16");
            #region LinqBegin16

            List<int> list16 = new List<int>() { -1, -8, 2, -7, -4, -3, 9 };

            PrintList(list16);

            PrintList(list16.Where(item16 => item16 > 0).ToList());

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 17");
            #region LinqBegin17

            PrintList(list16.Where(item17 => item17 % 2 != 0).Distinct().ToList());

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 18");
            #region LinqBegin18

            PrintList(list16.Where(item18 => item18 % 2 == 0 && item18 < 0).Reverse().ToList());

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 19");
            #region LinqBegin19

            List<int> list19 = new List<int>() { 12, 52, -7, 2, 12, 52 };
            
            PrintList(list19.Where(item19 => item19 > 0 && item19 % 10 == 2).Reverse<int>().Distinct().Reverse().ToList());

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 20");
            #region LinqBegin20

            List<int> list20 = new List<int>() { 5, -20, 20, 15 };

            List<int> list201 = list20.Where(item20 => item20 >= 10 && item20 < 100).ToList();

            list201.Sort();
            PrintList(list201);

            #endregion

            Console.WriteLine(new string('*', 35) + " >> 22");
            #region LinqBegin22

            List<string> list22 = new List<string>() { "A1", "AAA", "WEE4", "ASSA8" };

            PrintList(list22.Where(item22 => item22.Length > 2 && item22[item22.Length - 1] >= 48 && item22[item22.Length - 1] <= 57).ToList());

            List<string> list2201 = list22.Where(item22 => item22.Length > 2 && item22[item22.Length - 1] >= 48 && item22[item22.Length - 1] <= 57).ToList();

            list2201.Sort();
            PrintList(list2201);

            List<string> list88 = new List<string>() { "ASD", "AA", "WEEFFSFS", "ASSA8" };

            list88.Sort(SortBy22);
            PrintList(list88);

            #endregion
            Console.WriteLine(new string('*', 35) + " >> 23");
            #region LinqBegin23

            List<int> list23 = new List<int>() { 1, 15, 18, 19, 21, 233, 25 };
                        
            List<int> list2301 = list23.Skip(4).Where(item23 => item23 % 2 != 0 && item23 > 9 && item23 < 100).ToList();

            list2301.Sort(SortBy23);
            PrintList(list2301);

            #endregion


            Console.ReadKey();
        }

        static int InputInt(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false || number <= 0 || number > 9);

            return number;
        }

        static List<int> GeneratreList(List<int> list, Random rnd)
        {
            list = new List<int>();

            for (int i = 0; i < 7; i++)
            {
                list.Add(rnd.Next(5, 10));
            }

            return list;
        }

        static void PrintList<T>(List<T> list)
        {
            foreach (T x in list)
            {
                Console.Write(x + " ");
            }
            Console.WriteLine();
        }

        static bool Search(int x, int d)
        {
            bool result = false;

            if (x <= 0)
            {
                result = false;
            }
            else
            {
                while (x != 0)
                {
                    if (x == d)
                    {
                        result = true;
                        break;
                    }
                    x /= 10;
                }
            }
            //587
            return result;
        }

        static int SortBy23(int y, int x)
        {
            if (x < y) return -1;
            else if (x > y) return 1;
            else return 0;
        }

        static int SortBy22(string y, string x)
        {
            if (x.Length > y.Length) return -1;
            else if (x.Length < y.Length) return 1;
            else return 0;
        }

    }
}
